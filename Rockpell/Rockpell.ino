int fanPin = 3;
long crossTime = 0;
long currentCycle = 0;
int dimming = 5500;
// Pin 13 has an LED connected on most Arduino boards.
int led = 13;
int innerFeeder = 12;
int buttonPin = 7;
int val = 0;     // variable to store the read value
				 // variables will change:
int ledState = 0;
int buttonState = 0;         // variable for reading the pushbutton status

void setup()
{
	// initialize the digital pin as an output to onboard led.
	pinMode(led, OUTPUT);
	// initialize the digital pin as an output to inner feeder relay.
	pinMode(innerFeeder, OUTPUT);
	// initialize the digital pin as an output to fan.
	pinMode(fanPin, OUTPUT);
	// initialize the digital pin as an input from fan.
	pinMode(2, INPUT);
	// initialize the digital pin as an input from button.
	pinMode(buttonPin, INPUT);
	// turn off fan.
	digitalWrite(fanPin, 0);
	// turn the LED off by making the voltage LOW
	digitalWrite(led, ledState);
	// turn the inner feeder relay off by making the voltage LOW
	digitalWrite(innerFeeder, HIGH);
}

void loop() {
	//  if(digitalRead(2)){ 
	//    zeroCross(); 
	//  } 

	val = digitalRead(8);   // read the input pin
	digitalWrite(led, val);    // sets the LED to the button's value

							   // read the state of the pushbutton value:
	buttonState = digitalRead(buttonPin);
	// check if the pushbutton is pressed.
	// if it is, the buttonState is HIGH:
	if (buttonState == HIGH) {
		// turn LED on:    
		digitalWrite(innerFeeder, LOW);
	}
	else {
		// turn LED off:
		digitalWrite(innerFeeder, HIGH);
	}

	if (digitalRead(8)) {
		// turn the LED off by making the voltage LOW
		digitalWrite(led, LOW);

	}
	else
	{
		// turn the LED on (HIGH is the voltage level)
		digitalWrite(led, HIGH);
		if (digitalRead(2)) {
			zeroCross();
		}

	}
}

void zeroCross() {
	crossTime = micros();
	while ((micros() - crossTime) < 10000) {
		currentCycle = micros() - crossTime;
		if (currentCycle > dimming) digitalWrite(fanPin, 1);
		digitalWrite(fanPin, 0);
	}
}

